# p0

```bash
projectId="34213003"
jobId="2162859503"
curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" "https://gitlab.com/api/v4/projects/${projectId}/jobs/${jobId}/trace"

# get details of the job
curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" "https://gitlab.com/api/v4/projects/${projectId}/jobs/${jobId}"
```


## Get Bridges pipelines of a pipeline

```bash
projectId="34213003"
pipelineId="484419970"
curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" "https://gitlab.com/api/v4/projects/${projectId}/pipelines/${pipelineId}/bridges" | jq > bridges.json
```

### Get jobs for each bridge

```bash
projectId="484419979"
projecId="34212833"

curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" "https://gitlab.com/api/v4/projects/${projectId}/pipelines/${pipelineId}/jobs" | jq > pipeline.jobs.json

projectId="484419980"
projecId="34212818"

curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" "https://gitlab.com/api/v4/projects/${projectId}/pipelines/${pipelineId}/jobs" | jq > pipeline.jobs.json
```